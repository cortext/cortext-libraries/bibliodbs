# Bibliodbs

Bibliodbs' goal is to allow users to easily download records from large bibliographic databases, extract information from them, and clean up common defects.

It currently supports three databses: OurResearch's OpenAlex; Clarivate's WebOfScience (WOS); and Overton's policy document index.

You'll need API access keys for these databases in order to use bibliodbs.

## Installation

``` bash
python3 -m pip install git+https://gitlab.com/cortext/cortext-libraries/bibliodbs.git
```

## Usage

See the [docstring for the module](src/bibliodbs/__init__.py).

## General features

Mass downloading:
- [x] Paginate the API to get all records
- [x] Throttle downloads to respect API limits
- [x] Automated retry in case of server failure
- [x] Relaunch interrupted downloads to pick up from the same point

Cleaning:
- [x] Remove copyright "watermarks" at the end of a text

## OpenAlex

Support:
- [x] Get records from a query's API URL
- [x] Extract commonly used fields (ids, title, year, authors, journal, organizations etc) as a dictionary that can be passed to `pandas.DataFrame`
- [x] Get data for referenced works

Links:
- [OpenAlex technical documentation](https://docs.openalex.org/)

## WOS

Support:
- [x] Get records from a query
- [x] Get records from a dictionary of queries
- [x] Extract commonly used fields (ids, title, year, authors, journal, organizations etc) as a dictionary that can be passed to `pandas.DataFrame`
- [ ] Get data for referenced works

Links:
- [Web of Science API Expanded](https://developer.clarivate.com/apis/wos)


### Obtaining an API key for WOS

You'll need to create a project on their developer portal, then write an e-mail requesting a key. You will be asked to describe your usage and to list your institutions to check they've paid for access.

## Overton

Support:
- [x] Get records for a query
- [x] Get records from a dictionary of queries
- [x] Specify query parameters (e.g. source_country)
- [x] Get citation data (included in regular query)
- [x] Load data as a `pandas.DataFrame`, optionally flattening nested fields ("source" and "cites")

Links:
- [Using the Overton API](https://help.overton.io/article/using-the-overton-api/)

### Obtaining an API key for Overton

Check you have API access by making a search then hovering over "Export". If "Generate API call" is present, click on that and your API key will be shown as part of the destination URL.

## License

Bibliodbs
Copyright (C) 2021-2023 Cortext

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

