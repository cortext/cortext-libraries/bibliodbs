#!/usr/bin/env python

import logging
import re

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def make_remove_editor_copyright_watermark():
    """
    Some editors choose to harm researchers with the practice of placing copyright notices
    in the midst of metadata such as abstract text. This function tries to get
    rid of as many cases as possible with little risk of damaging the abstract.
    """

    def rg_or(*args):
        return r"(?:" + r"|".join(rf"(?:{x})" for x in args) + r")"

    def rg_ul(arg):
        return r"".join(rf"[{x.upper()}{x.lower()}]" for x in arg)

    num_final_characters = 200
    re_flags = r"(?x)"

    # units
    re_ss = r"\s*"
    re_pre_space = r"(?<=\s)"
    re_pre_space_or_dot = r"(?<![^\s\.])"
    re_post_space = r"(?=\s)"
    re_post_space_or_comma = r"(?![^\s,])"
    re_no_rsb = r"[^\]]"

    # elements
    re_copyright = r"\b" + rg_ul(r"copyright") + r"\S*"
    re_c = re_pre_space_or_dot + r"\(" + rg_ul(r"c") + r"\)" + re_post_space
    re_year = re_pre_space + r"\d{4}" + re_post_space_or_comma

    # copyright
    re_fuzzy_mark = r"(?: \(?\w\) | \? )"
    re_copy = rf"{re_copyright} (?: \s+ {re_fuzzy_mark} )?"
    re_mark = rf"{re_c}"
    re_copymark = rf"{rg_or(re_copy, re_mark)}"
    re_copymark_dated = rf"{re_copymark} {re_ss} {re_year}"

    # brackets
    re_copy_brackets = rf"\[ {re_no_rsb}* {re_copymark_dated} {re_no_rsb}* \] \.? \Z"
    re_end_brackets = rf"\[ {re_no_rsb}* \] \Z"
    re_correct = r"\b" + rg_or(rg_ul("correction"), rg_ul("corrected")) + r"\b"
    re_correction = rf"\[ {re_no_rsb}* {re_correct} {re_no_rsb}* \] \.? \Z"
    re_brackets = rg_or(re_end_brackets, re_copy_brackets, re_correction)

    # published
    re_ublished = rg_ul("ublished") + r"\s*" + rg_ul("by")
    re_pub = rf"{re_copymark} .{{,100}} [Pp]{re_ublished}"
    re_published = rg_or(re_pub, rf"P{re_ublished}")

    # remove
    re_remove = r"\s*" + rg_or(
        re_brackets,
        re_copymark_dated,
        re_published,
    )
    rx_remove = re.compile(re_flags + re_remove)

    def remove_editor_copyright_watermark(text, item_id=""):
        for match in rx_remove.finditer(text):
            if (start := match.start()) >= (len(text) - num_final_characters):
                logger.info(f"Matched ({item_id}): {match.group()}")
                return text[:start]
            else:
                logger.warning(
                    f"Matched ({item_id}) early at index {start}: {match.group()}"
                )
        return text

    return remove_editor_copyright_watermark


remove_editor_copyright_watermark = make_remove_editor_copyright_watermark()
