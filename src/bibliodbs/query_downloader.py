from abc import ABC, abstractmethod
from functools import wraps
from getpass import getpass
import gzip
from itertools import repeat
import json
from logging import getLogger
import os
from pathlib import Path
import time
from urllib.parse import urljoin

import httpx

logger = getLogger(__name__)

try:
    JSONL_SUFFIX = ".jsonl.zst"
    import pyzstd
except ImportError:
    logger.warning("Failed to import `pyzstd`, will use `gzip` compression.")
    JSONL_SUFFIX = ".jsonl.gz"


def c_open(filename, *args, **kwargs):
    if Path(filename).suffix == ".zst":
        return pyzstd.open(filename, *args, **kwargs)
    if Path(filename).suffix == ".gz":
        return gzip.open(filename, *args, **kwargs)
    return open(filename, *args, **kwargs)


def _rate_limited_method(func):
    @wraps(func)
    def wrapped(self, *args, **kwargs):
        time_debt = self._api_time_per_call - (
            time.time() - self._api_last_call_time.get()
        )
        time.sleep(max(0, time_debt))
        res = func(self, *args, **kwargs)
        self._api_last_call_time.set(time.time())
        return res

    return wrapped


class CallTimeHandler:
    def __init__(self, call_time):
        self._call_time = call_time

    def get(self):
        return self._call_time

    def set(self, value):
        self._call_time = value

    @classmethod
    def make_from(cls, other):
        if isinstance(other, CallTimeHandler):
            return other
        else:
            return CallTimeHandler(other)


class QueryDownloader(ABC):
    """
    Abstract base class for query downloader types
    (outpath) path of the JSONL file to be appended to.
    (api_last_call_time) allows coordinating instances to avoid throttling.
    """

    def __init__(
        self,
        *,
        outpath: [Path, str] = None,
        api_last_call_time: [CallTimeHandler, int] = 0,
        http_client=None,
    ):
        self._api_last_call_time = CallTimeHandler.make_from(api_last_call_time)
        self._api_time_per_call = 1
        self._http_method = "get"
        self._query_id = None
        self._records_found = None
        self._max_records = None
        if http_client is None:
            self._http_client = httpx
        else:
            self._http_client = http_client
        # arguments
        self._outpath = None if outpath is None else Path(outpath)
        if self._outpath is None:
            logger.warning(
                "Value of `outpath` is `None`: storing/downloading won't work."
            )
        elif self._outpath.exists():
            self.store_records = None
            logger.warning(
                "Given `outpath` already exists: storing/downloading has been DISABLED."
            )

    @_rate_limited_method
    def _wrapped_post(self, endpoint, params, headers=None):
        """
        Only WOS uses post for now and it takes json. May later need parametrization.
        """
        return self._http_client.post(endpoint, json=params, headers=headers)

    @_rate_limited_method
    def _wrapped_get(self, endpoint, params, headers=None):
        return self._http_client.get(endpoint, params=params, headers=headers)

    def get_records(self, first=1):
        if self._http_method == "get":
            return self._api_common(get_or_post=self._wrapped_get, first=first)
        if self._http_method == "post":
            return self._api_common(get_or_post=self._wrapped_post, first=first)

    def get_records_with_retry(self, first=1):
        errors = 0
        while True:
            try:
                return self.get_records(first)
            except (InternalServerError, self._http_client.ReadTimeout) as e:
                errors = errors + 1
                logger.warning(f"Handled exception ({errors}) at `first={first}`: {e}")
                time.sleep(2 ** min(errors, 9))

    def store_records(self, records):
        """Writes `records` to disk by appending to a JSONL file"""
        with c_open(self._outpath, "at") as file:
            for rec in records:
                file.writelines([json.dumps(rec), "\n"])

    def download(self, *, first=1, last=None):
        """
        Get all records and store them.
        """
        if (
            (self._max_records is not None)
            and (self.records_found > self._max_records)
            and (last > self._max_records)
        ):
            raise ValueError(
                f"Too many records found: {self.records_found} of {self._max_records}"
                " maximum allowed records for a query. Try breaking the query into"
                " multiple subqueries."
            )
        while True:
            recs = self.get_records_with_retry(first)
            if last is not None:
                recs = recs[: last + 1 - first]
            self.store_records(recs)
            first += len(recs)
            logger.info(f"{first - 1} of {self.records_found} records.")
            # Check if we're done
            if hasattr(self, "_cursor"):
                if self._cursor is None:
                    break
            else:
                if not recs:
                    logger.warning("Received empty record list before reported total.")
                    break
                if first > self.records_found:
                    break
                if last is not None and first > last:
                    break

    def _api_common(self, get_or_post, first=1):
        """
        Sends query_id or query to get records.
        """
        if self._query_id is None:
            return self._api_common_by_query(get_or_post=get_or_post, first=first)
        else:
            return self._api_get_by_query_id(self._wrapped_get, first=first)

    @abstractmethod
    def _api_common_by_query(self, get_or_post, first=1):
        """
        Sends query to get records.
        If supported by the API, stores the query id in `self._query_id`.
        """
        pass

    @abstractmethod
    def _api_get_by_query_id(self, get, first=1):
        """
        Uses the query id to get records.
        If unsupported by API, concrete classes should still implement this as `pass`.
        """
        pass

    @property
    def records_found(self):
        """
        The latest known total number of records.
        Make the first call if needed, but do not update the cursor if present.
        """
        if self._records_found is None:
            if hasattr(self, "_cursor"):
                cursor = self._cursor
            self.get_records_with_retry()
            if hasattr(self, "_cursor"):
                self._cursor = cursor
        return self._records_found


class OpenAlexQueryDownloader(QueryDownloader):
    """
    Downloads JSON records from OpenAlex using its API.

    Currently only supports passing a query URL, which can be obtained from
    a search on <https://openalex.org/>.

    API documentation: <https://docs.openalex.org/>

    FIXME: Parameter `first` has no effect with this downloader.
    """

    _API_URL = "https://api.openalex.org"
    _CURSOR_INIT = "*"

    def __init__(
        self,
        email_address,
        query_url,
        outpath=None,
        api_last_call_time=0,
        http_client=None,
        **api_params,
    ):
        super().__init__(
            outpath=outpath,
            api_last_call_time=api_last_call_time,
            http_client=http_client,
        )
        self._email_address = email_address
        self._query_url = query_url
        self._api_params = api_params
        self._cursor = self._CURSOR_INIT
        self._records_per_page = 100

    def _get_query_url(self):
        query_url = (
            self._query_url
            if self._query_url.startswith("http")
            else urljoin(self._API_URL, self._query_url)
        )
        mail_parameter = f"&mailto={self._email_address}"
        cursor_parameter = f"&cursor={self._cursor}"
        query_url = f"{query_url}{mail_parameter}{cursor_parameter}"
        return query_url

    def _api_common_by_query(self, get_or_post, first=1):
        url = self._get_query_url()
        params = {"per_page": self._records_per_page}
        if self._cursor == self._CURSOR_INIT:
            logger.debug(url)
            div, mod = divmod(first - 1, self._records_per_page)
            params["page"] = div + 1
        else:
            mod = 0
        res = get_or_post(url, params=params)
        res = res.json()
        meta, records = res["meta"], res["results"]
        self._records_found = meta["count"]
        self._cursor = meta["next_cursor"]
        return records[mod:]

    def _api_get_by_query_id(self, get, first):
        """
        Sends query id to get records. Unsupported by the API.
        """
        pass


class WOSQueryDownloader(QueryDownloader):
    """
    Downloads JSON records from Web of Science RESTful API

    OpenAPI:
    https://api.clarivate.com/swagger-ui/?url=https%3A%2F%2Fdeveloper.clarivate.com%2Fapis%2Fwos%2Fswagger
    """

    _WOS_API = "https://wos-api.clarivate.com/api/wos"

    def __init__(
        self,
        api_key,
        query,
        outpath=None,
        api_last_call_time=0,
        http_client=None,
        database_id="WOK",
        **api_params,
    ):
        super().__init__(
            outpath=outpath,
            api_last_call_time=api_last_call_time,
            http_client=http_client,
        )
        self._api_key = api_key
        self._query = query
        self._database_id = database_id
        self._api_params = api_params
        # hardcoded but adjustable
        self._http_method = "post"
        self._records_per_request = 100
        logger.debug(f"{self._query}\n{self._api_params}")

    def _api_common_by_query(self, get_or_post, first=1):
        headers = {"X-ApiKey": self._api_key}
        endpoint = f"{self._WOS_API}/"
        params = {
            "databaseId": self._database_id,
            "usrQuery": self._query,
            "count": self._records_per_request,
            "firstRecord": first,
        }
        for key, value in self._api_params.items():
            if value is not None:
                params[key] = value
        logger.debug(params)
        res = get_or_post(endpoint, params=params, headers=headers)
        self.check_status(res)
        res = res.json()
        self._query_id = res["QueryResult"]["QueryID"]
        self._records_found = res["QueryResult"]["RecordsFound"]
        recs = res["Data"]["Records"]["records"]
        return [] if recs == "" else recs["REC"]

    def _api_get_by_query_id(self, get, first):
        headers = {"X-ApiKey": self._api_key}
        endpoint = f"{self._WOS_API}/query/{self._query_id}"
        params = {
            "count": self._records_per_request,
            "firstRecord": first,
        }
        for key, value in self._api_params.items():  # is this necessary?
            if value is not None:
                params[key] = value
        res = get(endpoint, params=params, headers=headers).json()
        recs = res["Records"]["records"]
        return [] if recs == "" else recs["REC"]

    def check_status(self, res):
        if res.status_code == 500:
            logger.debug(err := res.json())
            raise InternalServerError(err)
        if res.status_code != 200:
            logger.error(err := res.json())
            raise RequestError(err)


class OvertonQueryDownloader(QueryDownloader):
    _OVERTON_API = "https://app.overton.io"

    def __init__(
        self,
        api_key,
        query,
        query_params=None,
        outpath=None,
        api_last_call_time=0,
        http_client=None,
    ):
        super().__init__(
            outpath=outpath,
            api_last_call_time=api_last_call_time,
            http_client=http_client,
        )
        self._api_key = api_key
        self._query = query
        self._query_params = {} if query_params is None else query_params
        # hardcoded by Overton, not adjustable
        self._records_per_request = 20
        self._max_query_length = 8074
        self._max_pages = 1000
        self._max_records = self._records_per_request * self._max_pages

    def _api_common_by_query(self, get_or_post, first=1):
        endpoint = f"{self._OVERTON_API}/documents.php"
        div, mod = divmod(first - 1, self._records_per_request)
        page = div + 1
        params = {
            "query": self._query,
            "sort": "relevance",
            "format": "json",
            "api_key": self._api_key,
            "page": page,
            **self._query_params,
        }
        logger.debug(params)
        res = get_or_post(endpoint, params=params)
        res = res.json()
        self._records_found = res["query"]["total_results"]
        recs = res["results"]
        return recs[mod:]

    def _api_get_by_query_id(self, get, first=1):
        """
        Sends query id to get records. Unsupported by the API.
        """
        pass


class APIKey:
    """
    Utility class to be imported in applications that ask the user for an API key so
    that the user is only asked once per seesion.
    """

    def __init__(self, default_key=None):
        self._api_key = None

    def get(self):
        if self._api_key is None:
            self._api_key = getpass("API key: ")
        return self._api_key

    def set(self, api_key):
        self._api_key = api_key


def download_queries(
    SomeQueryDownloader,
    api_key,
    filenames_query,
    data_dir,
    querydownloader_args={},
    last=None,
):
    """
    SomeQueryDownloader: any concrete subclass of QueryDownloader
    api_key: an API key for the database
    filenames_query: dictionary matching output files to the query to be stored
    data_dir: directory where to store the output files
    querydownloader_args: extra arguments passed to SomeQueryDownloader()

    Example `filenames_query` for `WOSQueryDownloader`:
    ```
    filenames_query = {
        'bob_1999': "AU=(Bob) AND PY=(1999)",
        'alice_2001': "AU=(Alice) AND PY=(2001)",
    }
    ```
    """
    data_dir = Path(data_dir)
    data_dir.mkdir(exist_ok=True)
    counts_dir = data_dir.with_name(data_dir.name + ".records_found")
    counts_dir.mkdir(exist_ok=True)
    api_last_call_time = CallTimeHandler(0)
    if isinstance(querydownloader_args, dict):
        querydownloader_args = repeat(querydownloader_args)
    name_query__args = zip(filenames_query.items(), querydownloader_args)
    for (query_name, query), querydownloader_args in name_query__args:
        outpath = data_dir / f"{query_name}{JSONL_SUFFIX}"
        if outpath.exists():
            logger.info(
                f"Skipping query '{query_name}' because '{outpath}' already exists"
            )
            continue
        logger.info(f"Downloading {query_name}")
        wqd = SomeQueryDownloader(
            api_key,
            query,
            outpath=outpath,
            api_last_call_time=api_last_call_time,
            **querydownloader_args,
        )
        wqd.download(last=last)
        (counts_dir / f"{query_name}").write_text(str(wqd.records_found))


def get_query_sizes(
    SomeQueryDownloader, api_key, names_queries, **querydownloader_args
):
    api_last_call_time = CallTimeHandler(0)
    queries_size = {}
    for name, query in names_queries.items():
        logger.info(f"Requesting query: {query}")
        wqd = SomeQueryDownloader(
            api_key,
            query,
            api_last_call_time=api_last_call_time,
            **querydownloader_args,
        )
        queries_size[name] = wqd.records_found
        logger.info(queries_size[name], "records found\n")
    return queries_size


def iter_records(dataset_path):
    with c_open(dataset_path) as file:
        for line in file:
            yield json.loads(line)


def merge_jsonl_datasets(merge_on_keys, dataset_paths, merged_path, dry=False):
    """
    Merge datasets keeping only the first record with same values for some keys.

    (merge_on_keys: list) Compare documents based on these keys
    (dataset_paths: list) Datasets to merge; directories in the list will be traversed
                          and all files included
    (merged_path: str|Path) where to store the merged dataset

    ->
    dry is True:
        (dataset_paths_deep: list): List of files to be merged.
    dry is False:
        File written to `merged_path`
        (merged_keys_values: list): Comparison keys of items discarded by merge.
    """
    if Path(merged_path).exists():
        raise ValueError
    dataset_paths_shallow = [Path(p) for p in dataset_paths]
    dataset_paths_deep = []
    for p in dataset_paths_shallow:
        if p.is_dir():
            for dirpath, dirnames, filenames in os.walk(p):
                dataset_paths_deep += [Path(dirpath, name) for name in filenames]
        else:
            dataset_paths_deep.append(p)
    if dry:
        return dataset_paths_deep
    merged_keys_values = []
    known_keys_values = set()
    with c_open(merged_path, "wt") as merged_file:
        for dataset_path in dataset_paths_deep:
            for obj in iter_records(dataset_path):
                keys_values = tuple(obj[key] for key in merge_on_keys)
                if keys_values in known_keys_values:
                    merged_keys_values.append(keys_values)
                else:
                    merged_file.writelines([json.dumps(obj), "\n"])
                    known_keys_values.add(keys_values)
    return merged_keys_values


def filter_jsonl_dataset(function, dataset_path, filtered_path):
    """
    Filter a dataset keeping only values for which `function` returns True
    (function) takes an object and returns boolean
    (dataset_path) dataset to filter
    (filtered_path) where to store the filtered dataset
    :int: the number of items discarded
    """
    if filtered_path.exists():
        raise ValueError
    num_discarded = 0
    with c_open(filtered_path, "wt") as filtered_file:
        for obj in iter_records(dataset_path):
            if function(obj):
                filtered_file.writelines([json.dumps(obj), "\n"])
            else:
                num_discarded += 1
    return num_discarded


def listit(x):
    return x if isinstance(x, list) else [x]


def single_or_none(iterable):
    """
    If `iterable`:
    - contains a single element, returns that;
    - is empty, returns None;
    - contains more than one element, raises ValueError.
    """
    x = iter(iterable)
    try:
        r = next(x)
    except StopIteration:
        return None
    try:
        _one_too_many = next(x)
    except StopIteration:
        return r
    raise ValueError("Expected a single item, got at least two:", r, _one_too_many)


class InternalServerError(Exception):
    """Server is at fault, continue trying"""

    pass


class RequestError(Exception):
    """We are at fault, stop trying"""

    pass
