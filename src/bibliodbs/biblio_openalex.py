from pathlib import Path
import logging
from itertools import tee
from tempfile import TemporaryDirectory
from urllib.parse import urlsplit

from .query_downloader import (
    OpenAlexQueryDownloader as QueryDownloader,
    CallTimeHandler,
    iter_records,
)
from . import query_writer
from itertools import islice

logger = logging.getLogger(__name__)


def extract_records(records):
    for record in records:
        re = RecordExtractor(record)
        yield re.extracted


def field(f):
    f.is_field = True
    return f


def multi_field(f):
    f.is_multi_field = True
    return f


class RecordExtractor:
    """
    Documentation for ClassName
    """

    fields_path = {
        "id": ["ids", "openalex"],
        "pmid": ["ids", "pmid"],
        "doi": ["ids", "doi"],
        "title": ["title"],
        "type": ["type"],
        "publication_year": ["publication_year"],
        "publication_date": ["publication_date"],
        "language": ["language"],
        "is_oa": ["open_access", "is_oa"],
        "oa_status": ["open_access", "oa_status"],
        "oa_url": ["open_access", "oa_url"],
        "source_id": ["primary_location", "source", "id"],
        "source_name": ["primary_location", "source", "display_name"],
        "source_type": ["primary_location", "source", "type"],
        "referenced_works": ["referenced_works"],
    }

    def __init__(self, record):
        self.record = record
        self.extracted = {}
        self._id = get_path(self.record, ["id"])
        self.extract()
        assert self._id == self.extracted["id"]

    def extract_by_path(self):
        for field, path in self.fields_path.items():
            try:
                self.extracted[field] = get_path(self.record, path)
            except Exception:
                logger.info(f"Missing data in <{self._id}>: {field}, from {path}")

    def extract_by_method(self):
        for name in dir(type(self)):
            attr = getattr(self, name)
            try:
                if getattr(attr, "is_field", False):
                    self.extracted[name] = attr()
                elif getattr(attr, "is_multi_field", False):
                    self.extracted.update(attr())
            except Exception:
                logger.info(f"Missing data in <{self._id}>: {name}")

    def extract(self):
        self.extract_by_path()
        self.extract_by_method()

    @field
    def abstract(self):
        aii = self.record["abstract_inverted_index"]
        len_abstract = max((x for xs in aii.values() for x in xs), default=-1) + 1
        abstract_l = [None] * len_abstract
        for term, indexes in aii.items():
            for index in indexes:
                abstract_l[index] = term
        abstract = " ".join(abstract_l)
        return abstract

    @multi_field
    def extract_authorships(self):
        fields = {}
        fields["authors_id"] = authors_id = []
        fields["authors_name"] = authors_name = []
        fields["authors_institutions_id"] = authors_institutions_id = []
        fields["authors_institutions_name"] = authors_institutions_name = []
        fields["authors_institutions_type"] = authors_institutions_type = []
        fields["authors_institutions_country"] = authors_institutions_country = []
        for authorship in self.record["authorships"]:
            author = authorship["author"]
            authors_id.append(author["id"])
            authors_name.append(author["display_name"])
            authors_institutions_id.append([])
            authors_institutions_name.append([])
            authors_institutions_type.append([])
            authors_institutions_country.append([])
            for institution in authorship["institutions"]:
                authors_institutions_id[-1].append(institution["id"])
                authors_institutions_name[-1].append(institution["display_name"])
                authors_institutions_type[-1].append(institution["type"])
                authors_institutions_country[-1].append(institution["country_code"])
        return fields


class RecordEnricher:
    """
    Collects data for works referenced in records.
    Records for referenced works are stored in `self.referenced_records`.
    """

    def __init__(self, email_address, cache={}):
        self.email_address = email_address
        self.referenced_records = cache
        # OpenAlex limits
        self.max_allowed_ids = 100

    def enrich_records(self, records):
        yield from self.expand_referenced_works(records)

    def expand_referenced_works(self, records):
        records, records_bis = tee(records)
        new_referenced_works = (
            oa_id
            for record in records
            for oa_id in record["referenced_works"]
            if oa_id not in self.referenced_records
        )
        self.load_works_by_id(
            new_referenced_works,
            select="authorships,primary_location,publication_year",
        )
        for record in records_bis:
            record["referenced_works_textual"] = []
            for oa_id in record["referenced_works"]:
                # Default None bc ids of deleted works remain in the refs of others
                ref_record = self.referenced_records.get(oa_id, None)
                try:
                    author_name = ref_record["authorships"][0]["author"]["display_name"]
                    author_parts = author_name.split()
                    author = author_last = author_parts[-1]
                    if author_firsts := "".join([x[0] for x in author_parts[:-1]]):
                        author = f"{author_last} {author_firsts}"
                    year = ref_record["publication_year"]
                    journal = ref_record["primary_location"]["source"]["display_name"]
                    value = f"{author}, {year}, {journal}"
                except Exception:
                    logger.warning(
                        f'Invalid referenced work in <{record["id"]}>: {oa_id}'
                    )
                    value = None
                record["referenced_works_textual"].append(value)
            yield record

    def load_works_by_id(self, openalex_ids, select=None):
        openalex_ids = iter(openalex_ids)
        select = "" if select is None else f"&select=ids,{select}"
        next_ids = [*islice(openalex_ids, self.max_allowed_ids)]
        api_last_call_time = CallTimeHandler(0)
        while next_ids:
            with TemporaryDirectory() as tmp_dir:
                outpath = Path(tmp_dir) / "records.jsonl.zst"
                # We need to `get_id` otherwise the API throws an error for URL length
                filter_ = "|".join(get_id(url) for url in next_ids)
                query_url = (
                    "https://api.openalex.org/works?"
                    f"filter=ids.openalex:{filter_}{select}"
                )
                qd = QueryDownloader(
                    email_address=self.email_address,
                    query_url=query_url,
                    outpath=outpath,
                    api_last_call_time=api_last_call_time,
                )
                qd.download()
                for record in iter_records(outpath):
                    self.referenced_records[record["ids"]["openalex"]] = record
            next_ids = [*islice(openalex_ids, self.max_allowed_ids)]


def get_path(obj, path):
    path = iter(path)
    try:
        first = next(path)
    except StopIteration:
        return obj
    return get_path(obj[first], path)


def get_id(url):
    return urlsplit(url).path


class QueryWriter(query_writer.QueryWriter):
    """
    https://docs.openalex.org/how-to-use-the-api/get-lists-of-entities/search-entities
    """

    @staticmethod
    def get_query_url(query, stem=False):
        stem_str = "" if stem else ".no_stem"
        url_tpl = (
            "https://api.openalex.org/works?filter=title_and_abstract.search{stem_str}"
            ":{query}"
            ",type:types/article|types/book-chapter|types/book,language:languages/en"
        )
        url = url_tpl.format(stem_str=stem_str, query=query)
        return url
