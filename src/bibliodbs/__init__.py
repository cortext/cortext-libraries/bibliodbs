"""
=========
bibliodbs
=========

- Download and consolidate bibliographic data.
- Data is stored in jsonl format, one line per record.

For OpenAlex:

```
import bibliodbs.query_downloader as qd

# Set up your email address
my_email_address = …

# Get a query url string for any search on <https://openalex.org> by clicking on the
# dented wheel and choosing "API query", then copying the string that appears.
my_query = "https://api.openalex.org/works?page=1&filter=default.search:coriander+OR+cilantro"

# Instantiate the class
oaqd = qd.OpenAlexQueryDownloader(
    email_address=my_email_address,
    query_url=my_query,
    outpath="./openalex_records.jsonl.zst",
)

# Print the number of records to be downloaded
print(oaqd.records_found)

# Download all records
oaqd.download()

# Alternatively, download only the first 1000 records
oaqd.download(last=1000)
```

For Web of Science (WOS):

Contact Clarivate and get an API key:
- https://developer.clarivate.com/apis/wos

```
wqd = qd.WOSQueryDownloader(
    api_key="your WOS API key",
    query="a valid WOS query",
    outpath="./wos_records.jsonl.gz",
)
wqd.download()
```

For Overton:

Get your API key as per:
- https://help.overton.io/article/using-the-overton-api/

```
oqd = qd.OvertonQueryDownloader(
    api_key="your Overton API key",
    query="a valid Overton query",
    outpath="./overton_records.jsonl.zst",
)
oqd.download()
```

Download multiple quries as multiple files:

```
qd.download_queries(
    qd.<SomeQueryDownloader>, # Any concrete subclass of QueryDownloader
    api_key="Your API key or email adress, according to SomeQueryDownloader",
    filenames_query={
        "file_name_N": "A query or API URL, according to SomeQueryDownloader",
        ...
    },
    data_dir="./record_store",
):
```

This will store records for "file_name_N" as "./record_store/file_name_N.jsonl.zstd".

To merge multiple files containing records and output a consolidated file, see the docstring for
 ```merge_jsonl_datasets()```.

To filter a file containing records and output a new one, see the docstring for
 ```filter_jsonl_dataset()```.

"""

__version__ = "0.2"
