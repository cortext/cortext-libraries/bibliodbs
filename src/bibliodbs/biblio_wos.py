import logging
from urllib.parse import quote_plus

from .query_downloader import (
    iter_records,
    listit,
    single_or_none,
)
from .biblio_clean import remove_editor_copyright_watermark
from . import query_writer

logger = logging.getLogger(__name__)


def load_extracted_cleaned_and_enriched(
    input_path, enriched=True, include_raw_records=False
):
    """
    (input_path) path to a JSONL file containing WOS records
    """
    for raw_record in iter_records(input_path):
        extracted = RecordExtractor(
            raw_record, include_raw_record=include_raw_records
        ).extract()
        uid = extracted["uid"]
        abstract_text = extracted["abstract"]
        if abstract_text is not None:
            extracted["abstract"] = remove_editor_copyright_watermark(
                abstract_text, item_id=uid
            )
        if enriched:
            Enrichments.apply_all(extracted)
        yield extracted


class RecordExtractor:
    def __init__(self, record, include_raw_record=False):
        self._include_raw_record = include_raw_record
        self.record = record
        self.identifier_data = record["dynamic_data"]["cluster_related"]["identifiers"]
        self.summary_data = record["static_data"]["summary"]
        self.meta_data = record["static_data"]["fullrecord_metadata"]
        self.extracted = {}

    def get_uid(self):
        return self.record["UID"]

    def get_pmid(self):
        pmid_candidates = (
            x["value"]
            for x in (
                listit(self.identifier_data["identifier"])
                if self.identifier_data
                else []
            )
            if x["type"] == "pmid"
        )
        return next(pmid_candidates, None)

    def get_title(self):
        return single_or_none(
            " ".join(listit(x["content"]))
            for x in self.summary_data["titles"]["title"]
            if x["type"] == "item"
            and x.get("lang_id", "en") == "en"
            and x.get("non_english", "N") == "N"
        )

    def get_doi(self):
        return next(
            (
                x["value"]
                for x in (
                    listit(self.identifier_data["identifier"])
                    if self.identifier_data
                    else []
                )
                if x["type"] in ("doi", "xref_doi")
            ),
            None,
        )

    def get_coll_id(self):
        return self.record["static_data"]["item"]["coll_id"]

    def get_doctypes(self):
        doctypes = listit(self.summary_data["doctypes"]["doctype"])
        return doctypes

    def get_normalized_doctypes(self):
        doctypes = listit(self.meta_data["normalized_doctypes"]["doctype"])
        return doctypes

    def get_year(self):
        return self.summary_data["pub_info"]["pubyear"]

    def get_venue(self):
        titles = self.summary_data["titles"]["title"]
        title_types = {"source", "source_abbrev", "abbrev_11"}
        venue = {}
        for title in titles:
            title_type = title["type"]
            if title_type in title_types and title_type not in venue:
                venue[f"venue_{title_type}"] = title["content"]
        venue["venue_source_mixed"] = venue.get(
            "venue_source_abbrev", venue["venue_source"]
        )
        return venue

    def get_authors(self):
        authors_wos_standard = []
        authors_daisng_id = []
        authors_full_name = []
        authors_wos_researcher_id = []
        for obj in listit(self.summary_data["names"]["name"]):
            if obj["role"] == "author":
                authors_wos_standard.append(obj.get("wos_standard", None))
                authors_daisng_id.append(obj.get("daisng_id", None))
                authors_full_name.append(obj.get("full_name", None))
                try:
                    """
                    'data-item-ids': {'data-item-id': {'type': 'person',
                      'content': 'KIV-3216-2024',
                      'id-type': 'PreferredRID'}},
                    """
                    wos_researcher_id = [
                        x["content"]
                        for x in listit(obj["data-item-ids"]["data-item-id"])
                    ]
                except Exception:
                    logger.info(f"Bad WOS Researcher ID: {obj}")
                    wos_researcher_id = None
                authors_wos_researcher_id.append(wos_researcher_id)
        return {
            "authors_wos_standard": authors_wos_standard,
            "authors_daisng_id": authors_daisng_id,
            "authors_full_name": authors_full_name,
            "authors_wos_researcher_id": authors_wos_researcher_id,
        }

    def extract(self):
        extracted = self.extracted
        meta_data = self.meta_data

        extracted["uid"] = self.get_uid()
        extracted["title"] = self.get_title()
        extracted["pmid"] = self.get_pmid()
        extracted["doi"] = self.get_doi()
        extracted["coll_id"] = self.get_coll_id()
        extracted["doctypes"] = self.get_doctypes()
        extracted["normalized_doctypes"] = self.get_normalized_doctypes()
        extracted["year"] = self.get_year()
        extracted.update(self.get_authors())
        extracted.update(self.get_venue())

        # Language
        extracted["language"] = single_or_none(
            lang["content"]
            for lang in listit(meta_data["languages"]["language"])
            if lang["type"] == "primary"
        )

        # Abstract
        extracted["abstract"] = single_or_none(
            " ".join(str(x) for x in listit(v))
            for metadata in ([meta_data] if "abstracts" in meta_data else [])
            for abstract in listit(metadata["abstracts"]["abstract"])
            for k, v in abstract["abstract_text"].items()
            if abstract.get("type", None) != "other"
            and abstract.get("lang_id", "en") == "en"
            and k != "count"
        )

        # Organizations
        organizations = []
        countries = []

        def format_org(org):
            return " ".join(listit(org["content"]))

        for address_name in listit(meta_data["addresses"].get("address_name", [])):
            if "country" in address_name["address_spec"]:
                countries.append(address_name["address_spec"]["country"])
            if "organizations" in address_name["address_spec"]:
                orgs = address_name["address_spec"]["organizations"]["organization"]
                y_orgs_f = [format_org(org) for org in orgs if org["pref"] == "Y"]
                if y_orgs_f:
                    organizations.extend(y_orgs_f)
                else:
                    organizations.extend([format_org(org) for org in orgs])
        # Store unique ordered values
        extracted["countries"] = [*{k: None for k in countries}]
        extracted["organizations"] = [*{k: None for k in organizations}]

        # Raw record
        if self._include_raw_record:
            extracted["raw_record"] = self.record
        return extracted


class Enrichments:
    @classmethod
    def get_url_doi(cls, record):
        if (doi := record["doi"]) is not None:
            return f"https://doi.org/{doi}"
        elif (title := record["title"]) is not None:
            return f"https://scholar.google.com/scholar?q={quote_plus(title)}"

    @classmethod
    def get_url_wos(cls, record):
        return f"https://www.webofscience.com/wos/alldb/full-record/{record['uid']}"

    @classmethod
    def apply_all(cls, record):
        record["url_doi"] = cls.get_url_doi(record)
        record["url_wos"] = cls.get_url_wos(record)


def add_urls_from_ids(df):
    """
    (df) a pandas.DataFrame ← pd.DataFrame([*load_extracted_and_cleaned(…)])
    """
    df["url_doi"] = df.agg(Enrichments.get_url_doi, axis=1)
    df["url_wos"] = df.agg(Enrichments.get_url_wos, axis=1)


class QueryWriter(query_writer.QueryWriter):
    """
    Field tags:
    http://webofscience.help.clarivate.com/en-us/Content/wos-core-collection/woscc-search-field-tags.htm
    """

    @classmethod
    def FIELD_EQ(cls, field, query):
        return f"{field}={cls.PAREN(query)}"
