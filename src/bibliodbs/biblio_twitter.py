from getpass import getpass
from html import unescape
import json
import logging
import re

import pandas as pd
import pyzstd

import tweepy

logger = logging.getLogger(__name__)

VERBOSE = False


"""
This module uses tweepy <https://www.tweepy.org/> to access the Twitter API.
It provides functions to store and load data, and to structure it as a Pandas DataFrame.
"""


####################
# Data acquisition #
####################

def get_twitter_api_client():
    try:
        bearer_token = TWITTER_API_BEARER_TOKEN
    except NameError:
        bearer_token = getpass("Please enter your Twitter API Bearer Token: ")
    return tweepy.Client(bearer_token, return_type=dict)


def api_get_counts(client, query, start_time, end_time, timezone="UTC"):
    pages = tweepy.Paginator(
        client.get_all_tweets_count,
        query=query,
        granularity="day",
        start_time=start_time,
        end_time=end_time,
    )
    res = [*pages.flatten()]
    counts = pd.DataFrame(res)
    date_columns = ["end", "start"]
    counts[date_columns] = counts[date_columns].transform(pd.to_datetime)
    counts = counts.set_index("start").tz_convert(timezone).resample("D").sum()
    return counts


def api_get_tweets_store_data_pages(client, query, start_time, end_time, out_path):
    tweet_fields = ["author_id", "created_at", "lang", "conversation_id"]
    more_tweet_fields = ["public_metrics", "referenced_tweets", "entities"]
    expansions = ["author_id", "geo.place_id"]
    user_fields = ["created_at", "public_metrics", "location", "description"]
    max_results = 500
    search_args = dict(
        query=query,
        start_time=start_time,
        end_time=end_time,
        tweet_fields=tweet_fields + more_tweet_fields,
        expansions=expansions,
        user_fields=user_fields,
        max_results=max_results,
    )
    pages = tweepy.Paginator(
        client.search_all_tweets,
        **search_args,
    )
    data_pages = [*pages]
    with pyzstd.open(out_path, "wt", level_or_option=19) as f:
        json.dump(data_pages, f)
    return data_pages


def reload_data_pages(fpath):
    with pyzstd.open(fpath, "r") as f:
        res = json.load(f)
    return res


def data_pages_to_dataframes(data_pages):
    tweets = pd.DataFrame(tweet for page in data_pages for tweet in page["data"])
    users = pd.DataFrame(
        user for page in data_pages for user in page["includes"]["users"]
    )
    users = users.drop_duplicates("id")
    places = pd.DataFrame(
        place for page in data_pages for place in page["includes"].get("places", [])
    )
    places = places.drop_duplicates("id")
    return {"tweets": tweets, "users": users, "places": places}


def get_id_translator(df, key):
    """
    Examples:
    author_id2username = get_id_translator(users, "username")
    place_id2full_name = get_id_translator(places, "full_name")
    """
    return df.set_index("id")[key].get


#####################
# Data manipulation #
#####################


class TweetTextCleaner:
    """
    To be used like (df: pd.DataFrame).agg(TweetTextCleaner.clean_text, axis=1)
    """
    # https://github.com/twitter/twitter-text/blob/master/js/src/regexp/validMentionOrList.js
    rx_username = re.compile(r"(?m)(?:^|\s)@[A-z0-9_]{1,20}(?!@)\b")
    rx_url = re.compile(r"(?m)(?:^|\s)https?://[\w/?=#\.]+")
    rx_monospace = re.compile(r" +")

    @classmethod
    def clean_text(cls, row):
        text = row["text"]
        # Remove unwanted entities
        entities = row["entities"]
        if hasattr(entities, "items"):
            limits = [
                [ent["start"], ent["end"]]
                for e_kind, ents in entities.items()
                for ent in ents
                if e_kind in {"urls", "mentions"}
            ]
            for start, end in limits:
                text = text[:start] + (end - start) * " " + text[end:]
        if VERBOSE:
            logger.warning("Cleaning entities missed by Twitter...")
        # Decode html escaped characters
        text = unescape(text)
        # some urls linger, may be linebreak related
        if VERBOSE and (res := cls.rx_url.findall(text)):
            logger.warning(f"{row.name}: {res}")
        text = cls.rx_url.sub(" ", text)
        # entities omit deleted accounts
        if VERBOSE and (res := cls.rx_username.findall(text)):
            logger.warning(f"{row.name}: {res}")
        text = cls.rx_username.sub(" ", text)
        # remove multiple whitspaces
        text = cls.rx_monospace.sub(" ", text)
        return text


################
# Simple stats #
################


def plot_tweet_count(counts, title=None):
    ax = counts.resample("M").sum().plot(y="tweet_count", title=title)
    ax.figure.tight_layout()
    for fmt in ("svg", "png", "pdf"):
        ax.figure.savefig(f"{title}_tweet_count.{fmt}")


def user_activity(tweets, users):
    user_activity = (
        tweets["author_id"]
        .value_counts()
        .reset_index()
        .transform(
            {
                "index": users.set_index("id")["username"].get,
                "author_id": lambda x: x,
            }
        )
    )
    user_activity = user_activity.rename({"author_id": "count"})
    return user_activity


def sum_tweet_counts_from_users(users):
    return users.public_metrics.map(lambda x: x["tweet_count"]).sum()
