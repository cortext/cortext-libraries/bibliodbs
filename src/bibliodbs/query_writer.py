class QueryWriter:
    """
    A basic writer providing syntax common to most others.
    """

    @classmethod
    def OR(cls, items):
        return " OR ".join(cls.PARENS(items))

    @classmethod
    def AND(cls, items):
        return " AND ".join(cls.PARENS(items))

    @classmethod
    def NOT(cls, item):
        return f"NOT {cls.PAREN(item)}"

    @staticmethod
    def PAREN(item):
        return f"({item})"

    @classmethod
    def PARENS(cls, items):
        for item in items:
            yield cls.PAREN(item)

    @staticmethod
    def QUOTE(item):
        return f'"{item}"'

    @classmethod
    def QUOTES(cls, items):
        for item in items:
            yield cls.QUOTE(item)
