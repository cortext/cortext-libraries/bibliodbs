from pathlib import Path
import pandas as pd
import json

from . import query_downloader


def read_records_as_dataframe(plain_records, flatten=("source", "cites", "highlights")):
    """
    (plain_records) iterable of json records (e.g. a file-like object for a JSONL file)
    (flatten) iterable of columns containing nested data to flatten
    """
    records = (json.loads(x) for x in plain_records)
    df = pd.DataFrame(records)
    if not flatten:
        return df
    for column in df.columns.intersection(flatten):
        if is_list_dict_deep(df[column]):
            flatten_list_dict(df[column], df, column)
        elif is_dict_deep(df[column]):
            column_keys = df[column].explode().dropna().unique()
            for key in column_keys:
                flat0 = df[column].map(lambda x: x[key])
                if is_list_dict_deep(flat0):
                    flatten_list_dict(flat0, df, f"{column}__{key}")
                else:
                    df[f"{column}__{key}"] = flat0
        else:
            raise ValueError(f"Can't flatten column {column}")
        del df[column]
    return df


def is_dict_deep(sr):
    """
    Checks if a data field from Overton is dict deep.
    """
    return sr.map(lambda x: isinstance(x, dict)).all()


def is_iterable_of_dicts(x):
    return all(isinstance(y, dict) for y in x)


def is_list_dict_deep(sr):
    """
    Checks if a data field from Overton is list[dict] deep.
    """
    return sr.dropna().map(is_iterable_of_dicts).all()


def flatten_list_dict(sr, df, prefix):
    keys = sr.explode().explode().dropna().unique()
    for key in keys:
        df[f"{prefix}__{key}"] = sr.map(lambda x: [y.get(key, None) for y in x])


def get_cortextmanager_schema(df):
    schema = dict.fromkeys(df.columns, [])
    for col in df.columns:
        first_valid_value = df[col].dropna().iloc[0]
        if isinstance(first_valid_value, list):
            schema[col] = ["rank"]
    return schema


def get_sources_last_added_record(api_key, source_ids, raw=False, http_client=None):
    data_dir = Path("source_first_by_added_on")
    data_dir.mkdir(exist_ok=True)
    sources_to_download = [
        x for x in source_ids if not (data_dir / f"{x}.jsonl").exists()
    ]
    filenames_query = {x: "" for x in sources_to_download}
    querydownloader_args = [
        {"query_params": {"source": x, "sort": "added_on"}, "http_client": http_client}
        for x in sources_to_download
    ]
    query_downloader.download_queries(
        SomeQueryDownloader=query_downloader.OvertonQueryDownloader,
        api_key=api_key,
        filenames_query=filenames_query,
        data_dir=data_dir,
        querydownloader_args=querydownloader_args,
        last=1,
    )

    def gen_data():
        for source_id in source_ids:
            with open(data_dir / f"{source_id}.jsonl") as fd:
                for line in fd:
                    yield line

    df = read_records_as_dataframe(gen_data())
    columns = ["source__source_id", "source__title", "added_on"]
    counts_dir = data_dir.with_name(data_dir.name + ".records_found")
    if counts_dir.exists():
        df["records_found"] = df["source__source_id"].map(
            lambda x: int((counts_dir / x).read_text())
        )
        columns += ["records_found"]
    return df if raw else df[columns]


def export_sources_last_added_record(api_key, source_ids, out_path, http_client=None):
    """
    Export sources_last_added_record to a spreadsheet.
    """
    df = get_sources_last_added_record(api_key, source_ids, http_client=http_client)
    df["added_on"] = pd.to_datetime(df.added_on)
    df.sort_values("source__title").to_excel(out_path, index=False)
