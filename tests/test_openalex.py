import pytest

import bibliodbs.biblio_openalex as boa


@pytest.fixture
def download(tmp_path):
    outpath = tmp_path / "records.jsonl.zst"
    query_url = (
        "https://api.openalex.org/works?"
        "page=1&filter=ids.openalex:W2007963058|W1975327940|W2951224618|W2171602344"
    )
    boa.QueryDownloader(
        email_address="tests@cortext.net",
        query_url=query_url,
        outpath=outpath,
    )
